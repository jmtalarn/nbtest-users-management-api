package com.nebulic.google.provisioning;

import java.io.FileInputStream;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import com.nebulic.google.parsing.UserFeedParser;
import com.nebulic.google.provisioning.response.AuthorizationClientResponse;
import com.nebulic.google.provisioning.response.CreateClientResponse;
import com.nebulic.google.provisioning.response.DeleteClientResponse;
import com.nebulic.google.provisioning.response.ListClientResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class GoogleProvisioningClient {

	 	private final Logger logger = LoggerFactory.getLogger(getClass());

	 	private static GoogleProvisioningClient ref;
	
	    private GoogleProvisioningClient(){  }

	    public static GoogleProvisioningClient getInstance()
	    {
	      if (ref == null){
	          ref = new GoogleProvisioningClient();
	      }
	      return ref;
	    }

	    
	    public AuthorizationClientResponse getAuthorization(String adminEmail, String password){
	    	AuthorizationClientResponse acr = new AuthorizationClientResponse();
	    	try {
	    		 
	    		Client client = Client.create();
	     
	    		WebResource webResource = client.resource(PrConstants.CLIENT_LOGIN);
	    		
	    		String input = PrConstants.CLIENT_LOGIN_PARAMS;
	    		input = input.replace(PrConstants.CLIENT_LOGIN_MAIL_TOKEN, URLEncoder.encode(adminEmail,PrConstants.UTF8));
	    		input = input.replace(PrConstants.CLIENT_LOGIN_PASSWORD_TOKEN, URLEncoder.encode(password,PrConstants.UTF8));
	    		
	    		ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED.toString())
	    		   .post(ClientResponse.class, input);
	    		
	    		acr.setStatusCode(response.getStatus());
	    		
	    		if (response.getStatus() == 200) {
	         		acr.setAuth_token(response.getEntity(String.class).split("Auth=")[1].replaceAll("\n","").replaceAll("\r",""));
	    		}

	    	  } catch (Exception e) {
	    		  logger.error("Error getting authorization",e);
	    	  }
	    	  return acr;  
	    	  
	    }

	    public ListClientResponse getUsers(String auth_token) {
		    ListClientResponse lcr = new ListClientResponse();
	    	try {
	    		 
	    		Client client = Client.create();
	     	    		
	    		WebResource webResource = client.resource(PrConstants.ALL_USERS);
	    		
	    		ClientResponse response = webResource.accept(MediaType.APPLICATION_ATOM_XML.toString())
	    									.header("Content-type",MediaType.APPLICATION_ATOM_XML.toString())
	    									.header("Authorization", "GoogleLogin auth=\""+auth_token+"\"")
	    									.get(ClientResponse.class);
	    		lcr.setStatusCode(response.getStatus());
	    		if (response.getStatus() == 200) {
		    		String output = response.getEntity(String.class);
		    		lcr.setUsers(UserFeedParser.parseUserFeed(output));
	    		}
	    		//We have to parse the response in order to return a List
	    	  }catch (Exception e) {
	    			  logger.error("Error getting users list" ,e);
	    	  }
	    	  return lcr;
	    } 

	    public CreateClientResponse createUser(String userName, String password, String hash,String familyName,String givenName, String auth_token) {
	    	CreateClientResponse ccr = new CreateClientResponse();
 	    	try {
	    		 
	    		Client client = Client.create();
	     	    		
	    		WebResource webResource = client.resource(PrConstants.CREATE_USER);
	    		
	    		FileInputStream fis = new FileInputStream(PrConstants.NEW_USER_ATOM_FILE);
	    		
	    		byte[] b = new byte[fis.available ()];
	    		fis.read(b);
	    		fis.close ();
	    		String userData = new String (b);
	    		
	    		 userData = userData.replace(PrConstants.USERNAME_TOKEN, userName)
	    				            .replace(PrConstants.PASSWORD_TOKEN,password)
	    				            .replace(PrConstants.HASH_TOKEN,hash)
	    				            .replace(PrConstants.FAMILYNAME_TOKEN,familyName)
	    				            .replace(PrConstants.GIVENNAME_TOKEN,givenName);
	    		
	    		
	    		ClientResponse response = webResource.type(MediaType.APPLICATION_ATOM_XML.toString())
	    									.accept(MediaType.APPLICATION_ATOM_XML.toString())
	    									.header("Content-type",MediaType.APPLICATION_ATOM_XML.toString())
	    									.header("Authorization", "GoogleLogin auth=\""+auth_token+"\"")
	    									.post(ClientResponse.class,userData);
	     
	    		if (response.getStatus() == 201) {
		    		String output = response.getEntity(String.class);
	    			ccr.setUser(UserFeedParser.parseUserFeed(output).get(0));
	    			
	    		}
	    		//We have to parse the response in order to return a List
	    		ccr.setStatusCode(response.getStatus());
	    	  }catch (Exception e) {
	    			  logger.error("Error getting users list" ,e);
	    	  }
	    	  return ccr;
	    } 

	    public DeleteClientResponse deleteUser(String userName,String auth_token) {
	    	DeleteClientResponse dcr = new DeleteClientResponse();
	    	try {
	    		 
	    		Client client = Client.create();
	     	    		
	    		WebResource webResource = client.resource(PrConstants.DELETE_USER+userName);
	    		
	    		ClientResponse response = webResource.type(MediaType.APPLICATION_ATOM_XML.toString())
	    									.accept(MediaType.APPLICATION_ATOM_XML.toString())
	    									.header("Content-type",MediaType.APPLICATION_ATOM_XML.toString())
	    									.header("Authorization", "GoogleLogin auth=\""+auth_token+"\"")
	    									.delete(ClientResponse.class);
	    		
	    		dcr.setStatusCode(response.getStatus());
	    		if (response.getStatus() == 200) {
	    			dcr.setUserName(userName);
	    		}
	    	  }catch (Exception e) {
	    			  logger.error("Error getting users list" ,e);
	    	  }
	    	  return dcr;
	    } 

}
