package com.nebulic.google.provisioning.response;

import com.nebulic.beans.User;

public class CreateClientResponse extends ClientResponse {

		private User user;

		
		public CreateClientResponse(){
			this.user = null;
			this.setStatusCode(0);
		}
		
		public CreateClientResponse(User user,int statusCode) {
			this.user = user;
			this.setStatusCode(statusCode);
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}
		
}
