package com.nebulic.google.provisioning.response;


public class AuthorizationClientResponse extends ClientResponse {
	
	private String auth_token;
	
	public AuthorizationClientResponse(){
		this.auth_token = null;
		this.setStatusCode(0);
	}
	
	public AuthorizationClientResponse(String auth_token,int statusCode) {
		this.setAuth_token(auth_token);
		this.setStatusCode(statusCode);
	}

	public String getAuth_token() {
		return auth_token;
	}

	public void setAuth_token(String auth_token) {
		this.auth_token = auth_token;
	}
}
