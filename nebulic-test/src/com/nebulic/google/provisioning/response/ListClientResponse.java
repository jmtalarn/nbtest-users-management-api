package com.nebulic.google.provisioning.response;

import java.util.List;

import com.nebulic.beans.User;

public class ListClientResponse extends ClientResponse {
	
	private List<User> users;
	
		
	public ListClientResponse(){
		this.users = null;
		this.setStatusCode(0);
	}
	
	public ListClientResponse(List<User> users,int statusCode) {
		this.users = users;
		this.setStatusCode(statusCode);
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
}
