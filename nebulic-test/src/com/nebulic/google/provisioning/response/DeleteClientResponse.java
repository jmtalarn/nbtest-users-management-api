package com.nebulic.google.provisioning.response;

public class DeleteClientResponse extends ClientResponse {

	
	private String userName;
	
	public DeleteClientResponse(){
		this.userName = null;
		this.setStatusCode(0);
	};
	
	public DeleteClientResponse(String userName,int statusCode) {
		this.userName = userName;
		this.setStatusCode(statusCode);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
