package com.nebulic.google.provisioning;

public class PrConstants {
	
	public static final String CLIENT_LOGIN = "https://www.google.com/accounts/ClientLogin";
	public static final String CLIENT_LOGIN_PARAMS = "&Email=<email_address>&Passwd=<password>&accountType=HOSTED&service=apps";
	public static final String CLIENT_LOGIN_MAIL_TOKEN = "<email_address>";
	public static final String CLIENT_LOGIN_PASSWORD_TOKEN = "<password>";
	//public static final String CLIENT_LOGIN_CONTENT_TYPE = "application/x-www-form-urlencoded";
	public static final String UTF8 = "UTF-8";
	
	public static final String ALL_USERS = "https://apps-apis.google.com/a/feeds/nebulic.com/user/2.0";
	
	public static final String CREATE_USER = "https://apps-apis.google.com/a/feeds/nebulic.com/user/2.0";
	public static final String DELETE_USER = "https://apps-apis.google.com/a/feeds/nebulic.com/user/2.0/";
	public static final String NEW_USER_ATOM_FILE = "resources/NewUserAtom.xml";
	public static final String USERNAME_TOKEN = "_userName";
	public static final String PASSWORD_TOKEN = "_password";
	public static final String HASH_TOKEN = "_hash";
	public static final String FAMILYNAME_TOKEN = "_familyName";
	public static final String GIVENNAME_TOKEN = "_givenName";
	
}
