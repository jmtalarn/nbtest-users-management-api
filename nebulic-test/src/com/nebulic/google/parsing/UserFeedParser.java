package com.nebulic.google.parsing;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.nebulic.beans.User;

public class UserFeedParser extends DefaultHandler {
	   private final Logger logger = LoggerFactory.getLogger(getClass());
       
	   private User user;
       private String temp;
       private ArrayList<User> userList = new ArrayList<User>();

		/** The main method sets things up for parsing */
		//public static void main(String[] args) throws IOException, SAXException,ParserConfigurationException {
		public static ArrayList<User> parseUserFeed(String inputUsersFeed)  throws IOException, SAXException,ParserConfigurationException {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser sp = parserFactory.newSAXParser();

            UserFeedParser handler = new UserFeedParser();
			InputStream xmlStream = new ByteArrayInputStream(inputUsersFeed.getBytes("UTF-8"));
			  
            //Finally, tell the parser to parse the input and notify the handler
            sp.parse(new InputSource(xmlStream), handler);

            return handler.getUserList();
        }
		public void startDocument() {
			logger.info(" *STARTING* parsing Users Atom Feed");
			userList = new ArrayList<User>();
		}

		public void endDocument() {
			logger.info(" Parsing Users Atom Feed *END*" );
		}

       /*
        * Per si interessa llegir el contingut d'un node.
		*/
       public void characters(char[] buffer, int start, int length) {
              temp = new String(buffer, start, length);
       }
      
       /*
        * Every time the parser encounters the beginning of a new element,
        * it calls this method, which resets the string buffer
        */ 
       public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
              temp = "";
              if (qName.equalsIgnoreCase("atom:entry")||qName.equalsIgnoreCase("entry")) {
                     user = new User();
              }else if (qName.equalsIgnoreCase("apps:login")){
					user.setUserName(attributes.getValue("userName"));
			  }else if (qName.equalsIgnoreCase("gd:who")){
					user.setEmail(attributes.getValue("email"));
			  }else if (qName.equalsIgnoreCase("apps:name")){
					user.setRealName(attributes.getValue("givenName")+" "+attributes.getValue("familyName"));
			  }
       }

       /*
        * When the parser encounters the end of an element, it calls this method
        */
       public void endElement(String uri, String localName, String qName)  throws SAXException {

              if (qName.equalsIgnoreCase("atom:entry")||qName.equalsIgnoreCase("entry")) {
                     // add it to the list
                    if ("".equals(user.getEmail())){
                    	user.setEmail(user.getUserName()+"@nebulic.com");
                    } 
            	  	userList.add(user);

              } 
			  /*
			  * Per utilitzar el contingut del node
			  *
			  *else if (qName.equalsIgnoreCase("Name")) {
              *       acct.setName(temp);
              *}
              */
       }
 

	   private ArrayList<User> getUserList(){
			return this.userList;
	   }
	
};