package com.nebulic.beans;


public class User {
	
	private String userName;
	private String email;
	private String realName;
	
	public User(String userName, String email, String realName){
		this.userName = userName;
		this.email = email;
		this.realName = realName;
	}
	
	public User() {
		this.userName = "";
		this.email = "";
		this.realName = "";
		
	}

	public String getUserName() {
		return this.userName;
	}
	public String getEmail() {
		return this.email;
	}
	public String getRealName() {
		return this.realName;
	}
	public void setUserName(String userName){
		this.userName = userName;
	}
	public void setEmail(String email){
		this.email = email;
	}
	public void setRealName(String realName){
		this.realName = realName;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("User Details - ");
		sb.append("Real Name : ").append(getRealName()).append(",");
		sb.append("Email : ").append(getEmail()).append(",");
		sb.append("User Name : ").append(getUserName()).append(".");
		return sb.toString();
	}
	

}