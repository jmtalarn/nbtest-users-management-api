package com.nebulic.spring.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nebulic.beans.User;
import com.nebulic.google.provisioning.GoogleProvisioningClient;
import com.nebulic.google.provisioning.response.AuthorizationClientResponse;
import com.nebulic.google.provisioning.response.CreateClientResponse;
import com.nebulic.google.provisioning.response.DeleteClientResponse;
import com.nebulic.google.provisioning.response.ListClientResponse;
import com.nebulic.utils.UtConstants;

@Controller
@RequestMapping("/management")
public class UsersController {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value="/authorization",method = RequestMethod.POST, produces = "application/json") 
	public @ResponseBody String getAuthorization(@RequestParam("userName") String userName,
										 @RequestParam("password") String password,
										 @Context final HttpServletResponse response) {
		logger.info("**************************** POST /authorization");
		
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
		AuthorizationClientResponse acr = gpc.getAuthorization(userName, password);
		
		response.setStatus(acr.getStatusCode());
		
		return acr.getAuth_token();
	}
	
	
	
	@RequestMapping(value="/users",method = RequestMethod.GET,produces = "application/json")
	public @ResponseBody List<User> listUsers(@RequestHeader("Authorization") String authorization,
												@Context final HttpServletResponse response) {
		logger.info("**************************** GET /list");
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
		String auth_token = getAuthToken(authorization);
		ListClientResponse lcr = gpc.getUsers(auth_token);
		
		response.setStatus(lcr.getStatusCode());
		
		return lcr.getUsers();
		
	};

	@RequestMapping(value="/user",method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody User addUser(@RequestHeader("Authorization") String authorization,
										@RequestParam("userName") String userName,
										@RequestParam("password") String password,
										@RequestParam("familyName") String familyName,
										@RequestParam("givenName") String givenName,
										@Context final HttpServletResponse response) {
		logger.info("**************************** POST /add");
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
		String auth_token = getAuthToken(authorization);
		
		CreateClientResponse ccr = gpc.createUser(userName, password, UtConstants.SHA1, familyName, givenName, auth_token);

		response.setStatus(ccr.getStatusCode());

		return ccr.getUser();
	}


	@RequestMapping(value="/delete/{userName}",method = RequestMethod.DELETE,produces = "application/json")
	public @ResponseBody String deleteUser(@PathVariable String userName,
											@RequestHeader("Authorization") String authorization,
											@Context final HttpServletResponse response) {
		logger.info("**************************** DELETE /delete/"+userName);
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
		String auth_token = getAuthToken(authorization);
		
		DeleteClientResponse dcr = gpc.deleteUser(userName,auth_token);
		response.setStatus(dcr.getStatusCode());
		
		return dcr.getUserName();
	}

	private String getAuthToken(String authorization) {
		
		return (authorization==null?"":authorization.split("auth=")[1].replaceAll("\n", "").replaceAll("\r",""));
	}
}