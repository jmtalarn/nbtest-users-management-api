package com.nebulic.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/*")
public class BasicController {


	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showMessage() {

		String message = "You are here because you don't called a API method."; 

		ModelAndView modelAndView = new ModelAndView("message");

		 modelAndView.addObject("message", message); 

		 return modelAndView;
	}

	
}