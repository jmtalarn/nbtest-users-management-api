package com.nebulic.google.provisioning;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.nebulic.google.provisioning.response.AuthorizationClientResponse;
import com.nebulic.utils.TestConstants;

public class GPClientGetAuthorizationOKTest {
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
		AuthorizationClientResponse acr = gpc.getAuthorization(TestConstants.ADMIN_NEBULIC_USER,TestConstants.ADMIN_NEBULIC_PWD);
		assertNotNull("Authorization should be true", acr.getAuth_token());

	}

}


