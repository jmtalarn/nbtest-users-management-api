package com.nebulic.google.provisioning;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nebulic.beans.User;
import com.nebulic.google.provisioning.response.AuthorizationClientResponse;
import com.nebulic.google.provisioning.response.ListClientResponse;
import com.nebulic.utils.TestConstants;

public class GPClientGetUsersOKTest {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
		AuthorizationClientResponse acr = gpc.getAuthorization(TestConstants.ADMIN_NEBULIC_USER,TestConstants.ADMIN_NEBULIC_PWD);
		assertNotNull("Authorization token doesn't be ", acr.getAuth_token());
		ListClientResponse lcr = gpc.getUsers(acr.getAuth_token());
		assertTrue("At least there must be 1 user", lcr.getUsers().size()>=1);
		for (User user: lcr.getUsers()){
			logger.info("*******" + user.toString());
		}
		

	}

}


