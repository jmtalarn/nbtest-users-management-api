package com.nebulic.google.provisioning;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.nebulic.beans.User;
import com.nebulic.google.provisioning.response.AuthorizationClientResponse;
import com.nebulic.google.provisioning.response.CreateClientResponse;
import com.nebulic.google.provisioning.response.DeleteClientResponse;
import com.nebulic.google.provisioning.response.ListClientResponse;
import com.nebulic.utils.Functions;
import com.nebulic.utils.TestConstants;
import com.nebulic.utils.UtConstants;

public class GPClientDeleteUserOKTest {
	
	String userName;
	String auth_token;
	@Before
	public void setUp() throws Exception {
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
		AuthorizationClientResponse acr = gpc.getAuthorization(TestConstants.ADMIN_NEBULIC_USER,TestConstants.ADMIN_NEBULIC_PWD);
		auth_token = acr.getAuth_token();
		ListClientResponse lcr = gpc.getUsers(auth_token);
		List<User> usersList = lcr.getUsers();
		String userPass = "password"+usersList.size();
			
		if (usersList.size()<10){
			CreateClientResponse ccr = gpc.createUser("userName"+usersList.size(), 
									  Functions.encryptPassword(userPass), 
				                      UtConstants.SHA1, 
				                      "familyName"+usersList.size(), 
				                      "givenName"+usersList.size(),
				                      acr.getAuth_token());
			userName = ccr.getUser().getUserName();
		}
	}

	@Test
	public void test() {
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();


		if (userName!=null && auth_token!=null){	
			DeleteClientResponse dcr = gpc.deleteUser(userName, auth_token);
			assertNotNull("If is deleted this must be true", dcr.getUserName());
		}

	}

}


