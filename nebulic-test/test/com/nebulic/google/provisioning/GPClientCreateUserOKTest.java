package com.nebulic.google.provisioning;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nebulic.beans.User;
import com.nebulic.google.provisioning.response.AuthorizationClientResponse;
import com.nebulic.google.provisioning.response.CreateClientResponse;
import com.nebulic.google.provisioning.response.ListClientResponse;
import com.nebulic.utils.Functions;
import com.nebulic.utils.TestConstants;
import com.nebulic.utils.UtConstants;

public class GPClientCreateUserOKTest {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	String userName;
	String authToken;
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void test() {
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
		AuthorizationClientResponse acr = gpc.getAuthorization(TestConstants.ADMIN_NEBULIC_USER,TestConstants.ADMIN_NEBULIC_PWD);
		assertNotNull("Authorization should not be null", acr.getAuth_token());
		ListClientResponse lcr =  gpc.getUsers(acr.getAuth_token());
		assertTrue("At least there must be 1 user", lcr.getUsers().size()>=1);
		
		String userPass = "password"+lcr.getUsers().size();
				
		if (lcr.getUsers().size()<10){
			CreateClientResponse ccr = gpc.createUser("userName"+lcr.getUsers().size(), 
									  Functions.encryptPassword(userPass), 
				                      UtConstants.SHA1, 
				                      "familyName"+lcr.getUsers().size(), 
				                      "givenName"+lcr.getUsers().size(),
				                      acr.getAuth_token());
			userName = ccr.getUser().getUserName();
			authToken = acr.getAuth_token();
			assertNotNull("If created the newUser is not null", ccr.getUser() );
			logger.info("****" + ccr.getUser().toString());
		}else{
			assertTrue("Maximum users assumed", lcr.getUsers().size()==10);
			
			CreateClientResponse ccr = gpc.createUser("userName"+lcr.getUsers().size(), 
					  Functions.encryptPassword(userPass), 
                      UtConstants.SHA1, 
                      "familyName"+lcr.getUsers().size(), 
                      "givenName"+lcr.getUsers().size(),
                      authToken);
			assertNull("No new user could be created", ccr.getUser());
		}

	}
	
	public void TearDown() throws Exception{
		if (userName!=null){
			GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
			gpc.deleteUser(userName, authToken);
			
		}
		
	}

}


