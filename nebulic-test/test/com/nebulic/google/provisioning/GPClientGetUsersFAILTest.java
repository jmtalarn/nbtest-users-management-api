package com.nebulic.google.provisioning;

import javax.xml.ws.Response;

import org.junit.Before;

import static org.junit.Assert.*; 

import com.nebulic.google.provisioning.response.ListClientResponse;
import javax.servlet.http.HttpServletResponse;

public class GPClientGetUsersFAILTest {
		
	@Before
	public void setUp() throws Exception {
	}


	public void test() {
		GoogleProvisioningClient gpc = GoogleProvisioningClient.getInstance();
		String fakeAuthToken = "fakeAuthToken";
		ListClientResponse lcr = gpc.getUsers(fakeAuthToken);
		assertEquals("The status code must be 401",lcr.getStatusCode(),HttpServletResponse.SC_UNAUTHORIZED);
	}

}


