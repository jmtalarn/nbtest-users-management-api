package com.nebulic.google.parsing;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.nebulic.beans.User;

public class UserFeedParserTest {
	
	public String userFeedXML = "";
	
	@Before
	public void setUp() throws Exception {
		FileInputStream fis = new FileInputStream("test-resources/usersFeed.xml");
		
		byte[] b = new byte[fis.available ()];
		fis.read(b);
		fis.close ();
		userFeedXML = new String (b);

	}

	@Test
	public void test() throws IOException, SAXException, ParserConfigurationException {
		
		ArrayList<User> userList = UserFeedParser.parseUserFeed(userFeedXML);
		assertTrue("There are two users in the list", userList.size()==2);
		
		String userNames = userList.get(0).getUserName() + " " + userList.get(1).getUserName();
		assertTrue("They are called JohnSmith and SusanJones", userNames.contains("JohnSmith")&&userNames.contains("SusanJones"));
		
	}

}
