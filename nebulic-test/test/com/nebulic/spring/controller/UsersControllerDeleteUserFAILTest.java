package com.nebulic.spring.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.nebulic.beans.User;
import com.nebulic.utils.Functions;



public class UsersControllerDeleteUserFAILTest  extends UsersControllerTestAuth{
	int previousTestsSize;
	String a_fake_auth_token= "A fake auth token!!";
	String newUserName = "newUserName888";
	@Before
	public void setUp() throws Exception {
		super.setUp();
		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
		    	.perform(get("/management/users").header("Authorization","Basic auth="+auth_token) );
		    		
			result.andExpect(status().isOk()).andExpect(content().mimeType(MediaType.APPLICATION_JSON_VALUE));
			
			MockHttpServletResponse response = result.andReturn().getResponse();

			ObjectMapper mapper = new ObjectMapper();
			     
			List<User> users = mapper.readValue(response.getContentAsString(), new TypeReference<List<User>>() {});
			previousTestsSize = users.size();
			
			MockMvcBuilders.standaloneSetup(new UsersController()).build()
	    	.perform(post("/management/user").header("Authorization","Basic auth="+auth_token)
	    				.param("userName",newUserName)
	    				.param("password", Functions.encryptPassword(newUserName))
	    				.param("familyName", "Family Name")
	    				.param("givenName", "Given Name"));
	}
	@Test
	public void testDeleteUserInvalidAuth() throws Exception {
		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
    	.perform(delete("/management/delete/"+newUserName).header("Authorization","Basic auth="+a_fake_auth_token));
		result.andExpect(status().isUnauthorized());
	}
	@Test
	public void testDeleteUserWithoutSomeParams() throws Exception {
		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
		    	.perform(delete("/management/delete/"+newUserName+"BUG").header("Authorization","Basic auth="+auth_token));
		result.andExpect(status().isBadRequest());
	}
	@Test
	public void testDeleteUserAfterAllNothing() throws Exception {
		
			ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
		    	.perform(get("/management/users").header("Authorization","Basic auth="+auth_token) );
		    		
			result.andExpect(status().isOk()).andExpect(content().mimeType(MediaType.APPLICATION_JSON_VALUE));
			
			MockHttpServletResponse response = result.andReturn().getResponse();

			ObjectMapper mapper = new ObjectMapper();
			     
			List<User> users = mapper.readValue(response.getContentAsString(), new TypeReference<List<User>>() {});
			
			assertEquals("The size of the users list must be the same", previousTestsSize,users.size());
	}
	
	public void TearDown() throws Exception{
		MockMvcBuilders.standaloneSetup(new UsersController()).build()
    	.perform(delete("/management/delete/"+newUserName).header("Authorization","Basic auth="+auth_token));
			
		}

}
