package com.nebulic.spring.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.nebulic.utils.TestConstants;



public class UsersControllerGetAuthorizationFAILTest {

	@Before
	public void setUp() {
	}

	@Test
	public void testAuthorizationWithGet() throws Exception {

		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
	    	.perform(get("/management/authorization")
					.param("userName", TestConstants.ADMIN_NEBULIC_USER)
					.param("password", TestConstants.ADMIN_NEBULIC_PWD));
			MockHttpServletResponse response = result.andReturn().getResponse();
			assertEquals("Http Status code must be 405 (Method not allowed)",response.getStatus(),HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	        
		
	}
	@Test
	public void testAuthorizationBadCredentials() throws Exception {

		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
	    	.perform(post("/management/authorization")
					.param("userName", TestConstants.ADMIN_NEBULIC_USER)
					.param("password", "WrongPassword"));
			MockHttpServletResponse response = result.andReturn().getResponse();
			assertEquals("Http Status code must be 403 (Forbidden)",response.getStatus(),HttpServletResponse.SC_FORBIDDEN);
	        
		
	}
	@Test
	public void testAuthorizationWithoutForm() throws Exception {

		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
	    	.perform(post("/management/authorization"));
			MockHttpServletResponse response = result.andReturn().getResponse();
			assertEquals("Http Status code must be 400 (Bad Request)",response.getStatus(),HttpServletResponse.SC_BAD_REQUEST);
	        
		
	}

	
}
