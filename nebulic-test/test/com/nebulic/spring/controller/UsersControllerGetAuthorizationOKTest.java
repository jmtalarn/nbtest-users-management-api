package com.nebulic.spring.controller;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.request.RequestPostProcessor;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.nebulic.utils.TestConstants;



public class UsersControllerGetAuthorizationOKTest {
	String auth_token;
	@Before
	public void setUp() throws Exception {

		
	}

	@Test
	public void testGetUsers() throws Exception {
		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
		    	.perform(post("/management/authorization")
						.param("userName", TestConstants.ADMIN_NEBULIC_USER)
						.param("password", TestConstants.ADMIN_NEBULIC_PWD));
		MockHttpServletResponse response = result.andReturn().getResponse();
		result.andExpect(status().isOk()).andExpect(content().mimeType(MediaType.APPLICATION_JSON_VALUE));
		
	}
	


}
