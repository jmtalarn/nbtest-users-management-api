package com.nebulic.spring.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.nebulic.beans.User;
import com.nebulic.utils.Functions;



public class UsersControllerCreateUserOKTest  extends UsersControllerTestAuth{
	String newUserName = "testUser444";
	@Test
	public void testCreateUser() throws Exception {
		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
	    	.perform(post("/management/user").header("Authorization","Basic auth="+auth_token)
	    				.param("userName",newUserName)
	    				.param("password", Functions.encryptPassword(newUserName))
	    				.param("familyName", "Family Name")
	    				.param("givenName", "Given Name"));
	    		
		result.andExpect(status().isCreated()).andExpect(content().mimeType(MediaType.APPLICATION_JSON_VALUE));
		
		MockHttpServletResponse response = result.andReturn().getResponse();

		ObjectMapper mapper = new ObjectMapper();
				     
		User user = mapper.readValue(response.getContentAsString(), new TypeReference<User>() {});
		assertEquals("The user name is the same",user.getUserName().toLowerCase(),newUserName.toLowerCase());

	}
	
	public void TearDown() throws Exception{
		MockMvcBuilders.standaloneSetup(new UsersController()).build()
    	.perform(delete("/delete/"+newUserName).header("Authorization","Basic auth="+auth_token));
			
		}


}
