package com.nebulic.spring.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.nebulic.beans.User;
import com.nebulic.utils.Functions;
import com.nebulic.utils.TestConstants;
import com.nebulic.utils.UtConstants;



public class UsersControllerDeleteUserOKTest  extends UsersControllerTestAuth{
	String newUserName = "testUser555";
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		MockMvcBuilders.standaloneSetup(new UsersController()).build()
    	.perform(post("/management/user").header("Authorization","Basic auth="+auth_token)
    				.param("userName",newUserName)
    				.param("password", Functions.encryptPassword(newUserName))
    				.param("familyName", "Family Name")
    				.param("givenName", "Given Name"));
	}
	@Test
	public void testDeleteUser() throws Exception {
		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
		    	.perform(delete("/management/delete/"+newUserName).header("Authorization","Basic auth="+auth_token));

	    		
		result.andExpect(status().isOk()).andExpect(content().mimeType(MediaType.APPLICATION_JSON_VALUE));
		
		MockHttpServletResponse response = result.andReturn().getResponse();

		String userName = response.getContentAsString();
		assertEquals("The user name of the deleted",userName,newUserName);

	}
	
	public void TearDown() throws Exception{
			
		}


}
