package com.nebulic.spring.controller;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

public class BasicControllerTest {
   @Test
   public void testBasicController() {
       BasicController c= new BasicController();
       ModelAndView mav= c.showMessage();
       Assert.assertEquals("message", mav.getViewName());
   }
}