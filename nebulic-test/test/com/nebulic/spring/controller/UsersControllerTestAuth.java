package com.nebulic.spring.controller;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.post;

import org.junit.Before;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.request.RequestPostProcessor;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.nebulic.utils.TestConstants;



public class UsersControllerTestAuth {
	String auth_token;
	@Before
	public void setUp() throws Exception {
		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
		    	.perform(post("/management/authorization")
						.param("userName", TestConstants.ADMIN_NEBULIC_USER)
						.param("password", TestConstants.ADMIN_NEBULIC_PWD));
		MockHttpServletResponse response = result.andReturn().getResponse();
		auth_token = response.getContentAsString();
		
	}




	/**
	 * Implementation of {@code RequestPostProcessor} with additional request
	 * building methods.
	 */
	protected static class HeaderRequestPostProcessor implements RequestPostProcessor {

		private HttpHeaders headers = new HttpHeaders();

		public HeaderRequestPostProcessor authorization(String value) {
			this.headers.add("Authorization", value);
			return this;
		}

		public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
			for (String headerName : this.headers.keySet()) {
				request.addHeader(headerName, this.headers.get(headerName));
			}
			return request;
		}
	}
	protected static HeaderRequestPostProcessor headers() {
		return new HeaderRequestPostProcessor();
	}

}