package com.nebulic.spring.controller;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.nebulic.beans.User;



public class UsersControllerGetUsersFailTest  extends UsersControllerTestAuth{
	String a_fake_token = "A INVALID TOKEN";
	
	@Test
	public void testGetUsersInvalidAuthToken() throws Exception {
		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
	    	.perform(get("/management/users").header("Authorization","Basic auth="+a_fake_token) );
	    		
		result.andExpect(status().isUnauthorized());
		
		MockHttpServletResponse response = result.andReturn().getResponse();

	}
	
	

}
