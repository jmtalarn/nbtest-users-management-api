package com.nebulic.spring.controller;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.server.ResultActions;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.nebulic.beans.User;



public class UsersControllerGetUsersOKTest  extends UsersControllerTestAuth{

	@Test
	public void testGetUsers() throws Exception {
		ResultActions result = MockMvcBuilders.standaloneSetup(new UsersController()).build()
	    	.perform(get("/management/users").header("Authorization","Basic auth="+auth_token) );
	    		
		result.andExpect(status().isOk()).andExpect(content().mimeType(MediaType.APPLICATION_JSON_VALUE));
		
		MockHttpServletResponse response = result.andReturn().getResponse();

		ObjectMapper mapper = new ObjectMapper();
		//mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		     
		List<User> users = mapper.readValue(response.getContentAsString(), new TypeReference<List<User>>() {});
		assertNotNull("The users list cannot be null", users);

	}
	

		

}
