<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!-- %@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%-->
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Nebulic Test</title>
<style>
	div.centered{
		width: 100%;
		text-aling: center;
	}
	dl dt {
		font-family: monospace;
	}
	div.code {font-family: monospace;}
	dl dt { margin-top: 2em;}
</style>
</head>

<body>
<div class="centered">
	<h1>~Nebulic Test - Users Management API~</h1>
</div>
<p class="centered">
	<b>${message}</b>
</p>
<p class="centered">
	This is the default page for the Nebulic Test Users Management API. Below this lines we found the API description.
	<dl>
	 <dt>POST http://localhost:8080/nebulic-test/management/authorization</dt>
    	<dd>First of all you an authentication token in order to do the rest of calls to API. 
    	You must provide in this call two parameters, the <b>userName</b> and the <b>password</b> in the form request 
    	and the method will return an authorization token.</dd>
	<dt>GET http://localhost:8080/nebulic-test/management/users</dt>
    	<dd>This method returns a JSON array of users objects. You need to include an <b>Authorization</b> header of this kind:
    		<div class="code">Authorization: Basic auth=<i>the returned authorization token</i></div>
    		The result will be with the following structure:
    		<div class="code">[
    							{ userName: <i>username 1</i>,
    							  email: <i>username1@nebulic.com</i>,
    							  realName: <i>Name1 SurName1</i> 
    							},
    							{ userName: <i>username 2</i>,
    							  email: <i>username2@nebulic.com</i>,
    							  realName: <i>Name2 SurName2</i> 
    							},
    							...
    						  ]
    		</div>
    	</dd>
	<dt>POST http://localhost:8080/nebulic-test/management/user</dt>
    	<dd>This method returns a JSON object for the user. You need to include an <b>Authorization</b> header of this kind:
    		<div class="code">Authorization: Basic auth=<i>the returned authorization token</i></div>
    		You need to send in the form request the information about the user to create. These are <b>userName</b>, 
    		<b>password</b> encoded with SHA-1, the <b>givenName</b> and the <b>familyName</b>.<br>
    		The result will be with the following structure:
    		<div class="code">	{ userName: <i>username 1</i>,
    							  email: <i>username1@nebulic.com</i>,
    							  realName: <i>Name1 SurName1</i> 
    							}
    		</div>
    	</dd>  
	 <dt>DELETE http://localhost:8080/nebulic-test/management/delete/<i>userName</i></dt>
    	<dd>This method returns a String with the userName of the user just deleted. You need to include an <b>Authorization</b> header of this kind:
    		<div class="code">Authorization: Basic auth=<i>the returned authorization token</i></div>
    	</dd>    	
	</dl>
</p>
</body>

</html>